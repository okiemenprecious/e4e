# Use a single-stage build with Python slim image
FROM python:3.9-slim

# Set working directory
WORKDIR /app

# Install pip-tools for managing requirements
RUN pip install pip-tools

# Copy and install Python requirements
COPY requirements/ /app/requirements/
RUN find /app/requirements -name '*.pip' -exec pip install --no-cache-dir -r {} \; \
    && find /app/requirements -name '*.in' -exec pip-compile {} \;

# Copy application code
COPY . /app

# Expose port 8000 (optional: for documentation purposes, doesn't publish the port)
EXPOSE 8000

# Default command to run the API
CMD ["uwsgi", "--ini", "/app/uwsgi.ini"]
